<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * LICENSE:
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @categories	Games/Entertainment, Systems Administration
 * @package		Bright Game Panel
 * @author		Xynergy <timo@zanzilan.be> @NOSPAM
 * @copyleft	2014
 * @license		GNU General Public License version 3.0 (GPLv3)
 * @version		Timo's Fork
 * @link		http://www.bgpanel.net/
 */

require("configuration.php");
require("includes/functions.php");
require("includes/mysql.php");

# Try to decode the document
$req = json_decode($HTTP_RAW_POST_DATA);

# Bail out when we don't have any (or bad) data
if($req == NULL) {
    http_response_code(400);
    echo "400 Bad Request";
    exit();
}

# Validate the API key
if (empty($req->apikey) || $req->apikey != APIKEY) {
    http_response_code(401);
    echo "401 Unauthorized";
    exit;
}

# Look up the hostname in the database
$box = query_fetch_assoc("SELECT * FROM `" . DBPREFIX . "box` WHERE `name` = '" . $req->hostname . "' LIMIT 1");

// query_fetch_assoc returns FALSE on empty data set
if(!$box) {
    # Console output
    echo "Adding new server $req->hostname to the database.\n";

    # If the box doesn't exist, add the box and its IP's to the database
    $ins = query_basic( "INSERT INTO `" . DBPREFIX . "box` SET
                        `name` = '". $req->hostname ."',
                        `ip` = '" . $_SERVER['REMOTE_ADDR'] . "',
                        `login` = '" . $req->user . "',
                        `password` = 'empty',
                        `sshport` = '" . $req->ssh_port . "',
                        `notes` = 'Automatically added by Announcer'" );

    echo "Successfully added $req->hostname to the database!\n";

    # Internal server error if this fails
    if ($ins == FALSE) {
        http_response_code(500);
        echo "500 Internal Server Error";
        exit;
    }

    # Get the insertId from the last query
    $boxid = mysql_insert_id();

    # Add the box' IP's
    foreach($req->ip_addr as $ip) {
        query_basic( "INSERT INTO `" . DBPREFIX . "boxIp` SET `boxid` = '" . $boxid . "', `ip` = '" . $ip->addr . "'" );

        # Log IP change to the event log
        $message = "Added IP $ip->addr to box $req->hostname";
        query_basic( "INSERT INTO `" . DBPREFIX . "log` SET `boxid` = '" . $boxid . "', `message` = '" . $message . "', `name` = '" . $req->hostname . "', `ip` = '" . $_SERVER['REMOTE_ADDR'] . "'" );
        echo $message . "\n";
    }

    # Initialize the server's cache
    $boxCache =	makeCache($boxid);
    query_basic( "UPDATE `" . DBPREFIX . "box` SET `cache` = '" . mysql_real_escape_string(gzcompress(serialize($boxCache), 2)) . "' WHERE `boxid` = '" . $boxid . "'" );

    # Write message to the event log
    $message = "Box Added: " . $req->hostname . " by Announcer";
    query_basic( "INSERT INTO `" . DBPREFIX . "log` SET `boxid` = '" . $boxid . "', `message` = '" . $message . "', `name` = '" . $req->hostname . "', `ip` = '" . $_SERVER['REMOTE_ADDR'] . "'" );
    echo $message . "\n";

} else {
    echo "Updating Server $req->hostname...\n";
    # If the server exists, update the SSH IP with the HTTP address and update all known IP's in the IP table
    if($box['ip'] != $_SERVER['REMOTE_ADDR']) {
        query_basic( "UPDATE `" . DBPREFIX . "box` SET `ip` = '" . $_SERVER['REMOTE_ADDR'] . "' WHERE `boxid` = '" . $box['boxid'] . "'" );
        $message = "Updated principal IP for " . $req->hostname . " to " . $_SERVER['REMOTE_ADDR'];
        query_basic( "INSERT INTO `" . DBPREFIX . "log` SET `boxid` = '" . $boxid . "', `message` = '" . $message . "', `name` = '" . $req->hostname . "', `ip` = '" . $_SERVER['REMOTE_ADDR'] . "'" );
    }

    # Get the box's IP's and check if we need to delete or insert any
    $ips = mysql_query("SELECT * FROM `" . DBPREFIX . "boxIp` WHERE `boxid` = '" . $box['boxid'] . "'");

    # Iterate over the IP's stored in the database
    while($rowip = mysql_fetch_assoc($ips)) {
        # Reset stale bit
        $ipstale = TRUE;

        # Iterate over IP's in the request
        foreach($req->ip_addr as $reqip) {
            if($rowip['ip'] == $reqip->addr)
                # Address match found, address is not stale
                $ipstale = FALSE;
        }

        # Address found to be stale; delete from database
        // if($ipstale) {
        //     if(FALSE != query_basic("DELETE FROM `" . DBPREFIX . "boxIp` WHERE `boxid` = '" . $box['boxid'] . "' AND `ip` = '" . $rowip['ip'] . "' LIMIT 1;" )) {
        //         $message = "Deleted stale IP " . $rowip['ip'] . " from box $req->hostname";
        //         query_basic( "INSERT INTO `" . DBPREFIX . "log` SET `boxid` = '" . $box['boxid'] . "', `message` = '" . $message . "', `name` = '" . $req->hostname . "', `ip` = '" . $_SERVER['REMOTE_ADDR'] . "'" );
        //         echo $message . "\n";
        //     }
        // }
    }

    # Push the box' IP's
    foreach($req->ip_addr as $v) {
        # This query doesn't filter boxId, we need to be able to steal IP's from other boxes
        $ip = query_fetch_assoc("SELECT * FROM `" . DBPREFIX . "boxIp` WHERE `ip` = '" . $v->addr . "' LIMIT 1");

        # Check if the IP has been found
        if($ip) {
            # Check if the IP belongs to the current box
            if($ip['boxid'] != $box['boxid']) {
                # Current IP belongs to a different box, update (steal) it
                echo "Assigning $v->addr to $req->hostname\n";
                if(FALSE != query_basic( "UPDATE `" . DBPREFIX . "boxIp` SET `boxid` = '" . $box['boxid'] . "' WHERE `ip` = '" . $v->addr . "'" )) {
                    $message = "IP " . $v->addr . " has been assigned to $req->hostname";
                    query_basic( "INSERT INTO `" . DBPREFIX . "log` SET `boxid` = '" . $box['boxid'] . "', `message` = '" . $message . "', `name` = '" . $req->hostname . "', `ip` = '" . $_SERVER['REMOTE_ADDR'] . "'" );
                    echo $message . "\n";
                }
            }
        } else {
            # Insert new IP
            echo "DEBUG: Adding ip $v->addr to $req->hostname\n";
            if(FALSE != query_basic( "INSERT INTO `" . DBPREFIX . "boxIp` SET `boxid` = '" . $box['boxid'] . "', `ip` = '" . $v->addr . "'" )) {
                $message = "IP " . $v->addr . " has been added to $req->hostname";
                query_basic( "INSERT INTO `" . DBPREFIX . "log` SET `boxid` = '" . $box['boxid'] . "', `message` = '" . $message . "', `name` = '" . $req->hostname . "', `ip` = '" . $_SERVER['REMOTE_ADDR'] . "'" );
                echo $message . "\n";
            }
        }
    }

    echo "Finished updating $req->hostname.\n";
}
